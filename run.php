<?php

require 'config.php';

//If $_POST values not empty, setting new values, if empty, using default from config.php
if (!empty($_POST["dir"]) && ($_POST["fname"])) {
	$dir = $_POST["dir"];
	$playlist_name = $_POST["fname"];
}

//If wrong file name entered to input, stopping script.
if (!file_exists("$playlist_name")) {
	echo"Error!<br>Playlist file: $playlist_name not found!";
	exit;	
}


//Data of songs playlist
$songs = file("$playlist_name");

$count_songs = count($songs);

//Counting, how many files in dir. 
$files = scandir('playlist_new');
$num_files = count($files) - 1;


//If file already exists, getting highest number in directory, and giving new value to $num_files
if (file_exists("playlist_new/playlist_$num_files.m3u8")) {
	
	$numbersarray = array();
	
	foreach ($files as &$hold) {
		$numbers = explode("_", $hold);
		
	if ( ! isset($numbers[1])) {
   $numbers[1] = null;
}
		$numbersall = explode(".", $numbers[1]);
		$numbersarray[] = "$numbersall[0]";
	}

	//Getting highest number in array and adding plus 1
	$num_files = max($numbersarray) +1;
}



//Writing corrected dir to new file.
		$fp = fopen("playlist_new/playlist_$num_files.m3u8", 'w');

foreach ($songs as &$in) {

	$write = str_replace("/<microSD1>/", "$dir", "$in");
		fwrite($fp, "$write");

}
		fclose($fp); 

	echo "New playlist file <b>playlist_new/playlist_$num_files.m3u8</b> created.<br><br>
	Songs corrected: $count_songs";



?>